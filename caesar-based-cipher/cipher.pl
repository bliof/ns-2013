#!/usr/bin/env perl
use strict;
use warnings;

use List::MoreUtils qw(uniq first_index);

MAIN: {
    my $decode = '';

    if (@ARGV == 4 and $ARGV[0] eq '-d') {
        $decode = 1;
        shift @ARGV;
    }

    if (@ARGV != 3) {
        print_help();
        exit -1;
    }

    my ($first_alphabet_string, $second_alphabet_string, $data) = @ARGV;

    my @first_alphabet = uniq split('', $first_alphabet_string);
    my @second_alphabet = uniq split('', $second_alphabet_string);

    if (@first_alphabet != @second_alphabet) {
        print_help();
        exit -1;
    }

    my $data_length = length $data;

    for my $char (split '', $data) {
        shuffle_alphabets(\@first_alphabet, \@second_alphabet, $data_length);

        my $char_position;

        if ($decode) {
            $char_position = first_index { $_ eq $char } @second_alphabet;
            print $first_alphabet[$char_position];
        } else {
            $char_position = first_index { $_ eq $char } @first_alphabet;
            print $second_alphabet[$char_position];
        }
    }

    print "\n";
}

sub shuffle_alphabets {
    my $first_alphabet = shift;
    my $second_alphabet = shift;
    my $data_length = shift;

    my $total_chars = @$first_alphabet;

    for my $i (0..($total_chars - 1)) {
        my $j = ($data_length + ord($first_alphabet->[$i]) + ord($second_alphabet->[$i])) % $total_chars;

        @$first_alphabet[$i, $j] = @$first_alphabet[$j, $i];
    }
}

sub print_help {
    print <<HELP
perl ./cipher.pl "\$(< first_alphabet)" "\$(< second_alphabet)" "\$(< file_to_encode)" > file_to_decode
perl ./cipher.pl -d "\$(< first_alphabet)" "\$(< second_alphabet)" "\$(< file_to_decode)"

The unique characters of the first alphabet should be the same amount as the ones of the second alphabet.
HELP
}
